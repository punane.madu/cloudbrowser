# Epitactic Cloud Browser: headless Chrome + image maps

Run a web browser in the cloud, accessed through your web browser

**Live demo**: [https://cloudbrowser.website/](https://cloudbrowser.website/).

[Announcement on Hacker News](https://news.ycombinator.com/item?id=19349447)

Inspired by
[The Firefox Experiments I Would Have Liked To Try: Cloud Browser](http://www.ianbicking.org/blog/2019/03/firefox-experiments-i-would-have-liked.html#cloud-browser) by Ian Bicking, as [discussed on Hacker News](https://news.ycombinator.com/item?id=19304802),
and the [Browsh HTML service at html.brow.sh](https://html.brow.sh).

The fully-fledged browser runs on a remote server, parsing HTML and running JavaScript to
render the page as normal. After loading completes, a screenshot is taken and an image map
is generated for each of the links. Not all websites work, but many can be browsed as usual
while your browser only displays the screenshot images, no JavaScript required.


## Installation

Built on [NodeJS](https://nodejs.org/en/) using [puppeteer](https://pptr.dev). To install
on a clean Ubuntu Linux 18.04 system:

```sh
apt -y install nodejs
apt -y install npm
npm install
node bib.js
```

If you get an error "node_modules/puppeteer/.local-chromium/linux-637110/chrome-linux/chrome: error while loading shared libraries: libX11.so.6: cannot open shared object file: No such file or directory", then X needs to be installed, the easiest way to do this on Ubuntu (for example on a VPS without a GUI) is:

```sh
apt -y install ubuntu-desktop
```

Now you can access the browser at http://localhost:3939/. (Optional) To setup a front-end web server:

```sh
apt -y install nginx
```

`/etc/nginx/sites-enabled/default`, add into `server` directive:

```
merge_slashes off;
location /b/ {
        proxy_pass http://127.0.0.1:3939/b/;
}
```

## License

Apache License, Version 2.0
