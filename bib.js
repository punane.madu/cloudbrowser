const http = require('http');
const puppeteer = require('puppeteer');
const fs = require('fs');
const querystring = require('querystring');
const cookie_parser = require('cookie');
const crypto = require('crypto');

const DEFAULT_HEIGHT = 800;
const DEFAULT_WIDTH = 600;
const DEFAULT_SCALE = 1;

const IMAGE_TYPE = 'png'; // png or jpeg
const IMAGE_QUALITY = 100; // 1 to 100 for jpeg

const CACHE_EXPIRATION_MS = 5*60*1000;
const INSTANT_LOAD = true; // if true, send expired cache immediately, with meta refresh to load fresh content

const URL_PREFIX = '/b'; // optional /path prefix for reverse proxies

const PENDING_GENERATION = {};

const server = http.createServer((req, res) => {
  let url = req.url;
  console.log(url);

  if (url.startsWith(URL_PREFIX)) {
    url = url.substring(URL_PREFIX.length);
  } else {
    res.writeHead(301, {
      'Content-Type': 'text/plain',
      'Location': `${URL_PREFIX}${url}`
    });
    res.end(`Must be accessed in path: ${URL_PREFIX}`);
    return;
  }

  if (url.startsWith('/')) {
    url = url.substring(1);
  }

  // TODO: is this needed? browsers may %-encode "< in address bar
  url = querystring.unescape(url);

  let width, height, scale;
  let requestID;

  // Cookie tells us the page dimensions to generate, if available
  if (req.headers.cookie) {
    console.log(req.headers.cookie);
    const cookie = cookie_parser.parse(req.headers.cookie);
    console.log(cookie);

    width = parseInt(cookie.w, 10);
    height = parseInt(cookie.h, 10);
    scale = parseInt(cookie.s, 10);

    if (isNaN(width)) width = DEFAULT_WIDTH;
    if (isNaN(height)) height = DEFAULT_HEIGHT;
    if (isNaN(scale)) scale = DEFAULT_SCALE;
  } else {
    width = DEFAULT_WIDTH;
    height = DEFAULT_HEIGHT;
    scale = DEFAULT_SCALE;
  }
  console.log(`Requested dimensions ${width}x${height}x${scale}`);

  if (url === '') {
    function sendIndex() {
      fs.readFile('index.html', 'utf8', (err, data) => {
        if (err) throw err;

        data = data.replace('%DIMENSIONS%', `${width}x${height}x${scale}`);

        res.writeHead(404, {
          'Content-Length': data.length,
          'Content-Type': 'text/html',
        });
        res.end(data);
      });
    }
    fs.access('index.html', fs.constants.F_OK, (err) => {
      if (err) {
        fs.copyFile('index.html.default', 'index.html', (err) => {
          if (err) throw err;

          sendIndex();
        });
      } else {
        sendIndex();
      }
    });
    return;
  }


  if (url.startsWith('http://') || url.startsWith('https://')) {
    const escapedURL = htmlspecialchars(url);

    const hash = crypto.createHash('sha256').update(url).digest('hex');
    const requestID = `${width}x${height}x${scale}_${hash}`;

    // Caching: check if last modified date on requestID below some expiration threshold, if exists, then return it instead
    fs.stat(`cache/${requestID}.html`, (err, stats) => {
      if (err) {
        generateNew();
      } else {
        const ageMilliseconds = Date.now() - stats.mtime;
        if (ageMilliseconds < CACHE_EXPIRATION_MS) {
          console.log(`Returned from cache since age ${ageMilliseconds} ms`);
          res.writeHead(200, {
            'Content-Type': 'text/html',
            'X-was-cached-age': ageMilliseconds, // TODO: some real cache header? freshness/expiry?
          });
          fs.readFile(`cache/${requestID}.html`, { encoding: 'utf8' }, (err, data) => {
            if (err) throw err;
            res.end(data.replace('<img src="', `<img src="${URL_PREFIX}/`)); // substituted here live so can serve locally
          });
        } else {
          if (INSTANT_LOAD && !PENDING_GENERATION[url]) {
            res.writeHead(200, {
              'Content-Type': 'text/html',
              'X-was-cached-age': ageMilliseconds, // TODO: some real cache header? freshness/expiry?
              'X-cache-expired': true,
            });
            fs.readFile(`cache/${requestID}.html`, { encoding: 'utf8' }, (err, data) => {
              if (err) throw err;
              res.end(data
                .replace('<head>',`<head><meta http-equiv="refresh" content="0; url=${URL_PREFIX}/${htmlspecialchars(url)}"><!-- expired cache age: ${ageMilliseconds} ms -->`)
                .replace('</body>',`<span style="position: fixed; top: 0px; left: 0px; z-index: 3;">${stats.mtime.toISOString()}</span></body>`) // show timestamp to indicate stale cache copy being refreshed TODO: local time?
              );
            });
            generateNew(true); // only save, don't reply for later
          } else {
            // blocking
            generateNew();
          }
        }
      }
    });

    function generateNew(background=false) {
      PENDING_GENERATION[url] = true;
      console.log(`Generating new requestID=${requestID}`);

      // Start sending the response as chunk-encoding immediately for responsiveness;
      // the rest is sent later in the callback when the image is generated
      if (!background) res.writeHead(200, {
        'Content-Type': 'text/html',
        // TODO: cache expire headers matching CACHE_EXPIRATION_MS?
      });

      const started = new Date();
      const head = `<!DOCTYPE html>
<html>
<head>
<!-- Generating response for ${requestID}, started at ${started.toUTCString()} -->
`;
      if (!background) res.write(head);
      // TODO: would be nice if could display some text, but how, we're still in the <head>
      // and the <title> won't be known yet until the page is generated (and non-JS is a feature)



      web2image(url, requestID, { width, height, deviceScaleFactor: scale })
      .then((metadata) => {
        console.log('Generated image, now sending shell');
        const finished = new Date();

        let imagemap = '<map name="imagemap">\n';
        for (const link of metadata.links) {
          imagemap += `<area shape="rect" coords="${link.x},${link.y},${link.width + link.x},${link.height + link.y}" href="${URL_PREFIX}/${htmlspecialchars(link.href)}" alt="${htmlspecialchars(link.href)}">\n`;
        }
        imagemap += '</map>';

        // HTML shell wrapper
        const body = `\
<!-- Took ${(finished - started) / 1000} seconds, completed at ${finished.toUTCString()} -->
<title>${metadata.title}</title>
<script>
console.log('Cookie before ',document.cookie);
console.log(\`Dimensions \${window.innerHeight}x\${window.innerWidth}x\${window.devicePixelRatio}\`);
document.cookie = 'h=' + window.innerHeight + '; path=/; max-age=2147483647';
document.cookie = 'w=' + window.innerWidth + '; path=/; max-age=2147483647';
document.cookie = 's=' + window.devicePixelRatio + '; path=/; max-age=2147483647';
console.log('Set cookie to', document.cookie);
</script>
<style>
img { position: absolute; top: 0; left: 0; }
@keyframes spinner {
  from { transform: translateY(-100px); }
  to { transform: translateY(0px); }
}
.loading {
  animation: spinner .6s ease 5;
  position: absolute;
}
</style>
</head>
<body>
<p>Loading ${escapedURL}<span class="loading">...</span></p>
${imagemap}
<img src="${URL_PREFIX}/${requestID}.${IMAGE_TYPE}" width="${width}" usemap="#imagemap">
</body>
</html>`;
        if (!background) res.end(body);
        fs.writeFile(`cache/${requestID}.html`, head + body, (err) => {
          delete PENDING_GENERATION[url];
          if (err) throw err;
        });
      })
      .catch((err) => {
        delete PENDING_GENERATION[url];
        console.log(url,err);
        // TODO: would like to return an HTTP error code... but the headers were already sent
        if (!background) res.end(`<b>Failed to render ${htmlspecialchars(url)}</b>: ${htmlspecialchars(''+err)}`);
      });
    }
  } else if (url.endsWith(`.${IMAGE_TYPE}`)) {
    requestID = url.substr(0, url.length - `.${IMAGE_TYPE}`.length);
    console.log(`Got image requestID=${requestID}`);

    if (!requestID.match(/^[0-9]+x[0-9]+x[0-9]+_[0-9a-f]+$/)) {
      res.writeHead(400, {
        'Content-Type': 'text/plain',
      });
      res.end('Invalid request ID');
      return;
    }

    fs.readFile(`cache/${requestID}.${IMAGE_TYPE}`, (err, data) => {
      if (err) {
        res.writeHead(401, {
          'Content-Type': 'text/plain',
        });
        res.end('File not found');
        return;
      }

      res.writeHead(200, {
        'Content-Length': data.length,
        'Content-Type': `image/${IMAGE_TYPE}`,
      });

      res.end(data);
    });
  } else {
    res.writeHead(400, {
      'Content-Type': 'text/plain',
    });
    res.end('Invalid URL');
  }
});
const port = 3939;
server.listen(port);
console.log(`Server is listening on http://localhost:${port}/`);

async function web2image(url, requestID, viewport) {
  let filename;
  filename = `cache/${requestID}.${IMAGE_TYPE}`;
  if (!fs.existsSync('cache')) fs.mkdirSync('cache');

  // If headless: false, then print to PDF promise rejects, why?
  // https://github.com/GoogleChrome/puppeteer/blob/v1.7.0/docs/api.md#pagepdfoptions 
  // "NOTE Generating a pdf is currently only supported in Chrome headless."
  const browser = await puppeteer.launch({headless: true});
  const page = await browser.newPage();
  await page.setViewport(viewport);
  await page.goto(url, {waitUntil: 'networkidle2'});
  //await page.pdf({path: 'hn.pdf', format: 'A4'}); // printed styling, may differ from interactive web browsing
  const options = {path: filename, fullPage: true, type: IMAGE_TYPE};
  if (IMAGE_TYPE === 'jpeg') options.quality = IMAGE_QUALITY;
  await page.screenshot(options);

  const metadata = {};
  metadata['url'] = url; // not used, but may be useful for debugging
  metadata['date'] = Date.now();
  metadata['title'] = await page.title();
  metadata['links'] = [];

  const linkElements = await page.$$('a');
  for (const linkElement of linkElements) {
    const href = await(await linkElement.getProperty('href')).jsonValue();
    const boundingBox = await linkElement.boundingBox();
    if (boundingBox) { // if omitted, then element is not visible - skip
      const { x, y, width, height } = boundingBox;
      //console.log(`href=${href}, boundingBox=${x} ${y} ${width} ${height}`);

      metadata['links'].push({ href, x, y, width, height });
    }
  }

  browser.close(); // don't wait for it

  return metadata;
}


// TODO: proper escaping function, querystring.escape() isn't it!
function htmlspecialchars(s) {
  s = s.replace(/"/g, '&quot;');
  s = s.replace(/</, '&lt;');
  s = s.replace(/`/, '&#96;');
  s = s.replace(/\$/, '&#36;');
  return s;
}


